package ru.t1.strelcov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.repository.IUserRepository;
import ru.t1.strelcov.tm.model.User;

import java.util.Optional;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return list.stream()
                .filter((user) -> login.equals(user.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        @NotNull final Optional<User> user = Optional.ofNullable(findByLogin(login));
        user.ifPresent(this::remove);
        return user.orElse(null);
    }

    @Override
    public boolean loginExists(@NotNull final String login) {
        return list.stream().anyMatch((user) -> login.equals(user.getLogin()));
    }

}
