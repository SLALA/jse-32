package ru.t1.strelcov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.strelcov.tm.api.service.IProjectService;
import ru.t1.strelcov.tm.api.service.ServiceLocator;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;
import ru.t1.strelcov.tm.enumerated.SortType;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.exception.system.IncorrectSortOptionException;

import java.util.Comparator;

public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    @NotNull
    @Override
    public ProjectListResponse listProject(@NotNull ProjectListRequest request) {
        check(request);
        return new ProjectListResponse(getProjectService().findAll(request.getUserId()));
    }

    @NotNull
    @Override
    public ProjectListSortedResponse listSortedProject(@NotNull ProjectListSortedRequest request) {
        check(request);
        final String sort = request.getSort();
        if (SortType.isValidByName(sort)) {
            @NotNull final SortType sortType = SortType.valueOf(sort);
            @NotNull final Comparator comparator = sortType.getComparator();
            return new ProjectListSortedResponse(getProjectService().findAll(request.getUserId(), comparator));
        } else
            throw new IncorrectSortOptionException(sort);
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIdResponse changeStatusByIdProject(@NotNull ProjectChangeStatusByIdRequest request) {
        check(request);
        return new ProjectChangeStatusByIdResponse(getProjectService().changeStatusById(request.getUserId(), request.getId(), Status.valueOf(request.getStatus())));
    }

    @NotNull
    @Override
    public ProjectChangeStatusByNameResponse changeStatusByNameProject(@NotNull ProjectChangeStatusByNameRequest request) {
        check(request);
        return new ProjectChangeStatusByNameResponse(getProjectService().changeStatusByName(request.getUserId(), request.getName(), Status.valueOf(request.getStatus())));
    }

    @NotNull
    @Override
    public ProjectClearResponse clearProject(@NotNull ProjectClearRequest request) {
        check(request);
        getProjectService().clear(request.getUserId());
        return new ProjectClearResponse();
    }

    @NotNull
    @Override
    public ProjectCreateResponse createProject(@NotNull ProjectCreateRequest request) {
        check(request);
        return new ProjectCreateResponse(getProjectService().add(request.getUserId(), request.getName(), request.getDescription()));
    }

    @NotNull
    @Override
    public ProjectFindByIdResponse findByIdProject(@NotNull ProjectFindByIdRequest request) {
        check(request);
        return new ProjectFindByIdResponse(getProjectService().findById(request.getUserId(), request.getId()));
    }

    @NotNull
    @Override
    public ProjectFindByNameResponse findByNameProject(@NotNull ProjectFindByNameRequest request) {
        check(request);
        return new ProjectFindByNameResponse(getProjectService().findByName(request.getUserId(), request.getName()));
    }

    @NotNull
    @Override
    public ProjectRemoveByIdResponse removeByIdProject(@NotNull ProjectRemoveByIdRequest request) {
        check(request);
        return new ProjectRemoveByIdResponse(getProjectService().removeById(request.getUserId(), request.getId()));
    }

    @NotNull
    @Override
    public ProjectRemoveByNameResponse removeByNameProject(@NotNull ProjectRemoveByNameRequest request) {
        check(request);
        return new ProjectRemoveByNameResponse(getProjectService().removeByName(request.getUserId(), request.getName()));
    }

    @NotNull
    @Override
    public ProjectRemoveWithTasksResponse removeWithTasksProject(@NotNull ProjectRemoveWithTasksRequest request) {
        check(request);
        return new ProjectRemoveWithTasksResponse(serviceLocator.getProjectTaskService().removeProjectById(request.getUserId(), request.getId()));
    }

    @NotNull
    @Override
    public ProjectUpdateByIdResponse updateByIdProject(@NotNull ProjectUpdateByIdRequest request) {
        check(request);
        return new ProjectUpdateByIdResponse(getProjectService().updateById(request.getUserId(), request.getId(), request.getName(), request.getDescription()));
    }

    @NotNull
    @Override
    public ProjectUpdateByNameResponse updateByNameProject(@NotNull ProjectUpdateByNameRequest request) {
        check(request);
        return new ProjectUpdateByNameResponse(getProjectService().updateById(request.getUserId(), request.getOldName(), request.getName(), request.getDescription()));
    }
}
