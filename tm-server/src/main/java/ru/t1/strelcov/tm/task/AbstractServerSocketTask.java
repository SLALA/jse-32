package ru.t1.strelcov.tm.task;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.component.Server;

import java.net.Socket;

@Setter
@Getter
public abstract class AbstractServerSocketTask extends AbstractServerTask {

    @NotNull
    protected final Socket socket;

    @Nullable
    protected String userId;

    public AbstractServerSocketTask(@NotNull Server server, @NotNull Socket socket) {
        super(server);
        this.socket = socket;
    }

    public AbstractServerSocketTask(@NotNull Server server, @NotNull Socket socket, @Nullable String userId) {
        super(server);
        this.socket = socket;
        this.userId = userId;
    }

}
