package ru.t1.strelcov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.strelcov.tm.api.service.ITaskService;
import ru.t1.strelcov.tm.api.service.ServiceLocator;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;
import ru.t1.strelcov.tm.enumerated.SortType;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.exception.system.IncorrectSortOptionException;

import java.util.Comparator;

public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    @NotNull
    @Override
    public TaskListResponse listTask(@NotNull TaskListRequest request) {
        check(request);
        return new TaskListResponse(getTaskService().findAll(request.getUserId()));
    }

    @NotNull
    @Override
    public TaskListSortedResponse listSortedTask(@NotNull TaskListSortedRequest request) {
        check(request);
        final String sort = request.getSort();
        if (SortType.isValidByName(sort)) {
            @NotNull final SortType sortType = SortType.valueOf(sort);
            @NotNull final Comparator comparator = sortType.getComparator();
            return new TaskListSortedResponse(getTaskService().findAll(request.getUserId(), comparator));
        } else
            throw new IncorrectSortOptionException(sort);
    }

    @NotNull
    @Override
    public TaskListByProjectIdResponse listByProjectIdTask(@NotNull TaskListByProjectIdRequest request) {
        check(request);
        return new TaskListByProjectIdResponse(serviceLocator.getProjectTaskService().findAllTasksByProjectId(request.getUserId(), request.getProjectId()));
    }

    @NotNull
    @Override
    public TaskChangeStatusByIdResponse changeStatusByIdTask(@NotNull TaskChangeStatusByIdRequest request) {
        check(request);
        return new TaskChangeStatusByIdResponse(getTaskService().changeStatusById(request.getUserId(), request.getId(), Status.valueOf(request.getStatus())));
    }

    @NotNull
    @Override
    public TaskChangeStatusByNameResponse changeStatusByNameTask(@NotNull TaskChangeStatusByNameRequest request) {
        check(request);
        return new TaskChangeStatusByNameResponse(getTaskService().changeStatusByName(request.getUserId(), request.getName(), Status.valueOf(request.getStatus())));
    }

    @NotNull
    @Override
    public TaskClearResponse clearTask(@NotNull TaskClearRequest request) {
        check(request);
        getTaskService().clear(request.getUserId());
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    public TaskCreateResponse createTask(@NotNull TaskCreateRequest request) {
        check(request);
        return new TaskCreateResponse(getTaskService().add(request.getUserId(), request.getName(), request.getDescription()));
    }

    @NotNull
    @Override
    public TaskFindByIdResponse findByIdTask(@NotNull TaskFindByIdRequest request) {
        check(request);
        return new TaskFindByIdResponse(getTaskService().findById(request.getUserId(), request.getId()));
    }

    @NotNull
    @Override
    public TaskFindByNameResponse findByNameTask(@NotNull TaskFindByNameRequest request) {
        check(request);
        return new TaskFindByNameResponse(getTaskService().findByName(request.getUserId(), request.getName()));
    }

    @NotNull
    @Override
    public TaskRemoveByIdResponse removeByIdTask(@NotNull TaskRemoveByIdRequest request) {
        check(request);
        return new TaskRemoveByIdResponse(getTaskService().removeById(request.getUserId(), request.getId()));
    }

    @NotNull
    @Override
    public TaskRemoveByNameResponse removeByNameTask(@NotNull TaskRemoveByNameRequest request) {
        check(request);
        return new TaskRemoveByNameResponse(getTaskService().removeByName(request.getUserId(), request.getName()));
    }

    @NotNull
    @Override
    public TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request) {
        check(request);
        return new TaskBindToProjectResponse(serviceLocator.getProjectTaskService().bindTaskToProject(request.getUserId(), request.getTaskId(), request.getProjectId()));
    }

    @NotNull
    @Override
    public TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull TaskUnbindFromProjectRequest request) {
        check(request);
        return new TaskUnbindFromProjectResponse(serviceLocator.getProjectTaskService().unbindTaskFromProject(request.getUserId(), request.getTaskId()));
    }

    @NotNull
    @Override
    public TaskUpdateByIdResponse updateByIdTask(@NotNull TaskUpdateByIdRequest request) {
        check(request);
        return new TaskUpdateByIdResponse(getTaskService().updateById(request.getUserId(), request.getId(), request.getName(), request.getDescription()));
    }

    @NotNull
    @Override
    public TaskUpdateByNameResponse updateByNameTask(@NotNull TaskUpdateByNameRequest request) {
        check(request);
        return new TaskUpdateByNameResponse(getTaskService().updateById(request.getUserId(), request.getOldName(), request.getName(), request.getDescription()));
    }

}
