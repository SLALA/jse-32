package ru.t1.strelcov.tm.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.component.Server;
import ru.t1.strelcov.tm.dto.request.AbstractRequest;
import ru.t1.strelcov.tm.dto.request.AbstractUserRequest;
import ru.t1.strelcov.tm.dto.request.UserLoginRequest;
import ru.t1.strelcov.tm.dto.request.UserLogoutRequest;
import ru.t1.strelcov.tm.dto.response.AbstractResponse;
import ru.t1.strelcov.tm.dto.response.ServerErrorResponse;
import ru.t1.strelcov.tm.exception.system.UnknownServerRequestException;
import ru.t1.strelcov.tm.model.User;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

public final class ServerRequestTask extends AbstractServerSocketTask implements Runnable {

    public ServerRequestTask(@NotNull final Server server, @NotNull final Socket socket) {
        super(server, socket);
    }

    public ServerRequestTask(@NotNull final Server server, @NotNull final Socket socket, @Nullable String userId) {
        super(server, socket, userId);
    }

    @Nullable AbstractRequest request;

    @Nullable AbstractResponse response;

    @Override
    public void run() {
        try {
            processInput();
            setUserId();
            processLogin();
            processRequest();
            processLogout();
        } catch (@NotNull Throwable throwable) {
            server.getBootstrap().getLoggerService().errors(throwable);
            response = new ServerErrorResponse(throwable);
        } finally {
            processOutput();
        }
    }

    @SneakyThrows
    private void processInput() {
        @NotNull final InputStream inputStream = socket.getInputStream();
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        @NotNull final Object inputObject = objectInputStream.readObject();
        request = (AbstractRequest) inputObject;
    }

    @SneakyThrows
    private void processOutput() {
        @Nullable final OutputStream outputStream = socket.getOutputStream();
        @Nullable final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(response);
        server.submit(new ServerRequestTask(server, socket, userId));
    }

    private void processRequest() {
        if (request == null) throw new UnknownServerRequestException(null);
        response = (AbstractResponse) server.call(request);
    }

    private void processLogin() {
        if (!(request instanceof UserLoginRequest)) return;
        @NotNull final UserLoginRequest loginRequest = (UserLoginRequest) request;
        @NotNull final User user = server.getBootstrap().getAuthService().check(loginRequest.getLogin(), loginRequest.getPassword());
        userId = user.getId();
    }

    private void processLogout() {
        if (!(request instanceof UserLogoutRequest)) return;
        userId = null;
    }

    private void setUserId() {
        if (request == null) throw new UnknownServerRequestException(null);
        if (request instanceof AbstractUserRequest)
            ((AbstractUserRequest) request).setUserId(userId);
    }

}
