package ru.t1.strelcov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.strelcov.tm.api.service.ServiceLocator;
import ru.t1.strelcov.tm.dto.request.UserLoginRequest;
import ru.t1.strelcov.tm.dto.request.UserLogoutRequest;
import ru.t1.strelcov.tm.dto.request.UserProfileRequest;
import ru.t1.strelcov.tm.dto.response.UserLoginResponse;
import ru.t1.strelcov.tm.dto.response.UserLogoutResponse;
import ru.t1.strelcov.tm.dto.response.UserProfileResponse;
import ru.t1.strelcov.tm.model.User;

public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }


    @NotNull
    @Override
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        return new UserLoginResponse();
    }

    @NotNull
    @Override
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    public UserProfileResponse getProfile(@NotNull final UserProfileRequest request) {
        check(request);
        @NotNull final User user = serviceLocator.getUserService().findById(request.getUserId());
        return new UserProfileResponse(user);
    }

}
