package ru.t1.strelcov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.endpoint.IDataEndpoint;
import ru.t1.strelcov.tm.api.service.ServiceLocator;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;
import ru.t1.strelcov.tm.enumerated.Role;

public final class DataEndpoint extends AbstractEndpoint implements IDataEndpoint {

    public DataEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public DataBase64LoadResponse loadBase64Data(@NotNull DataBase64LoadRequest request) {
        check(request, Role.ADMIN);
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    public DataBase64SaveResponse saveBase64Data(@NotNull DataBase64SaveRequest request) {
        check(request, Role.ADMIN);
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    public DataBinaryLoadResponse loadBinaryData(@NotNull DataBinaryLoadRequest request) {
        check(request, Role.ADMIN);
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    public DataBinarySaveResponse saveBinaryData(@NotNull DataBinarySaveRequest request) {
        check(request, Role.ADMIN);
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    public DataJsonJAXBSaveResponse saveJsonJAXBData(@NotNull DataJsonJAXBSaveRequest request) {
        check(request, Role.ADMIN);
        return new DataJsonJAXBSaveResponse();
    }

    @NotNull
    @Override
    public DataJsonJAXBLoadResponse loadJsonJAXBData(@NotNull DataJsonJAXBLoadRequest request) {
        check(request, Role.ADMIN);
        return new DataJsonJAXBLoadResponse();
    }

    @NotNull
    @Override
    public DataJsonFasterXMLLoadResponse loadJsonFasterXMLData(@NotNull DataJsonFasterXMLLoadRequest request) {
        check(request, Role.ADMIN);
        return new DataJsonFasterXMLLoadResponse();
    }

    @NotNull
    @Override
    public DataXmlFasterXMLLoadResponse loadXmlFasterXMLData(@NotNull DataXmlFasterXMLLoadRequest request) {
        check(request, Role.ADMIN);
        return new DataXmlFasterXMLLoadResponse();
    }

    @NotNull
    @Override
    public DataYamlFasterXMLSaveResponse saveYamlFasterXMLData(@NotNull DataYamlFasterXMLSaveRequest request) {
        check(request, Role.ADMIN);
        return new DataYamlFasterXMLSaveResponse();
    }

    @NotNull
    @Override
    public DataJsonFasterXMLSaveResponse saveJsonFasterXMLData(@NotNull DataJsonFasterXMLSaveRequest request) {
        check(request, Role.ADMIN);
        return new DataJsonFasterXMLSaveResponse();
    }

    @NotNull
    @Override
    public DataXmlFasterXMLSaveResponse saveXmlFasterXMLData(@NotNull DataXmlFasterXMLSaveRequest request) {
        check(request, Role.ADMIN);
        return new DataXmlFasterXMLSaveResponse();
    }

    @NotNull
    @Override
    public DataYamlFasterXMLLoadResponse loadYamlFasterXMLData(@NotNull DataYamlFasterXMLLoadRequest request) {
        check(request, Role.ADMIN);
        return new DataYamlFasterXMLLoadResponse();
    }

    @NotNull
    @Override
    public DataXmlJAXBLoadResponse loadXmlJAXBData(@NotNull DataXmlJAXBLoadRequest request) {
        check(request, Role.ADMIN);
        return new DataXmlJAXBLoadResponse();
    }

    @NotNull
    @Override
    public DataXmlJAXBSaveResponse saveXmlJAXBData(@NotNull DataXmlJAXBSaveRequest request) {
        check(request, Role.ADMIN);
        return new DataXmlJAXBSaveResponse();
    }

}
