package ru.t1.strelcov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.repository.IUserRepository;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.api.service.IUserService;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.empty.EmptyLoginException;
import ru.t1.strelcov.tm.exception.empty.EmptyPasswordException;
import ru.t1.strelcov.tm.exception.entity.UserAdminLockException;
import ru.t1.strelcov.tm.exception.entity.UserLoginExistsException;
import ru.t1.strelcov.tm.exception.entity.UserNotFoundException;
import ru.t1.strelcov.tm.model.User;
import ru.t1.strelcov.tm.util.HashUtil;

import java.util.Optional;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull final IUserRepository userRepository, @NotNull IPropertyService propertyService) {
        super(userRepository);
        this.userRepository = userRepository;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User add(@Nullable final String login, @Nullable final String password) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        if (userRepository.loginExists(login)) throw new UserLoginExistsException();
        @NotNull final String passwordHash = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), password);
        @NotNull final User user = new User(login, passwordHash);
        add(user);
        return user;
    }

    @NotNull
    @Override
    public User add(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        if (userRepository.loginExists(login)) throw new UserLoginExistsException();
        @NotNull final String passwordHash = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), password);
        @NotNull final User user = new User(login, passwordHash, email);
        add(user);
        return user;
    }

    @NotNull
    @Override
    public User add(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        if (userRepository.loginExists(login)) throw new UserLoginExistsException();
        @NotNull final User user;
        @NotNull final String passwordHash = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), password);
        user = new User(login, passwordHash, role);
        add(user);
        return user;
    }

    @NotNull
    @Override
    public User findByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        return Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
    }

    @NotNull
    @Override
    public User removeByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        return Optional.ofNullable(userRepository.removeByLogin(login)).orElseThrow(UserNotFoundException::new);
    }

    @NotNull
    @Override
    public User updateById(@Nullable final String id, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName, @Nullable final String email) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final User user = findById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User updateByLogin(@Nullable final String login, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName, @Nullable final String email) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        @NotNull final User user = findByLogin(login);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        return user;
    }

    @Override
    public void changePasswordById(@Nullable final String id, @Nullable final String password) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        @NotNull final User user = findById(id);
        final String passwordHash = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), password);
        user.setPasswordHash(passwordHash);
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        @NotNull final User user = findByLogin(login);
        if (Optional.ofNullable(user.getRole()).filter((i) -> i.equals(Role.ADMIN)).isPresent())
            throw new UserAdminLockException();
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        @NotNull final User user = findByLogin(login);
        user.setLocked(false);
    }

}
