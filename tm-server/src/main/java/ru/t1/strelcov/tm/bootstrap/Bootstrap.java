package ru.t1.strelcov.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.*;
import ru.t1.strelcov.tm.api.repository.IProjectRepository;
import ru.t1.strelcov.tm.api.repository.ITaskRepository;
import ru.t1.strelcov.tm.api.repository.IUserRepository;
import ru.t1.strelcov.tm.api.service.*;
import ru.t1.strelcov.tm.component.Backup;
import ru.t1.strelcov.tm.component.Server;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.endpoint.*;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.repository.ProjectRepository;
import ru.t1.strelcov.tm.repository.TaskRepository;
import ru.t1.strelcov.tm.repository.UserRepository;
import ru.t1.strelcov.tm.service.*;
import ru.t1.strelcov.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final IDataService dataService = new DataService(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IDataEndpoint dataEndpoint = new DataEndpoint(this);

    @NotNull
    private final Server server = new Server(this);


    private void initUsers() {
        userService.add("test", "test", Role.USER);
        userService.add("admin", "admin", Role.ADMIN);
    }

    private void initData() {
        @Nullable String userId = userService.findByLogin("test").getId();
        @Nullable String projectId = projectService.add(userId, "p1", "p1").getId();
        projectService.add(userId, "p2", "p2");
        taskService.add(userId, "t1", "t1").setProjectId(projectId);
        taskService.add(userId, "t2", "t2").setProjectId(projectId);
        userId = userService.findByLogin("admin").getId();
        projectId = projectService.add(userId, "pa1", "pa1").getId();
        projectService.add(userId, "pa2", "pa2");
        taskService.add(userId, "a1", "a1").setProjectId(projectId);
        taskService.add(userId, "a2", "a2").setProjectId(projectId);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initServer() {
        server.register(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.register(ServerVersionRequest.class, systemEndpoint::getVersion);

        server.register(UserLoginRequest.class, authEndpoint::login);
        server.register(UserLogoutRequest.class, authEndpoint::logout);
        server.register(UserProfileRequest.class, authEndpoint::getProfile);

        server.register(UserListRequest.class, userEndpoint::listUser);
        server.register(UserUpdateByLoginRequest.class, userEndpoint::updateUserByLogin);
        server.register(UserLockByLoginRequest.class, userEndpoint::lockByLogin);
        server.register(UserRegisterRequest.class, userEndpoint::registerUser);
        server.register(UserRemoveByLoginRequest.class, userEndpoint::removeUserByLogin);
        server.register(UserUnlockByLoginRequest.class, userEndpoint::unlockByLogin);
        server.register(UserChangePasswordRequest.class, userEndpoint::changePassword);

        server.register(ProjectListRequest.class, projectEndpoint::listProject);
        server.register(ProjectCreateRequest.class, projectEndpoint::createProject);
        server.register(ProjectClearRequest.class, projectEndpoint::clearProject);
        server.register(ProjectUpdateByNameRequest.class, projectEndpoint::updateByNameProject);
        server.register(ProjectUpdateByIdRequest.class, projectEndpoint::updateByIdProject);
        server.register(ProjectListSortedRequest.class, projectEndpoint::listSortedProject);
        server.register(ProjectRemoveByNameRequest.class, projectEndpoint::removeByNameProject);
        server.register(ProjectRemoveByIdRequest.class, projectEndpoint::removeByIdProject);
        server.register(ProjectFindByNameRequest.class, projectEndpoint::findByNameProject);
        server.register(ProjectFindByIdRequest.class, projectEndpoint::findByIdProject);
        server.register(ProjectRemoveWithTasksRequest.class, projectEndpoint::removeWithTasksProject);
        server.register(ProjectChangeStatusByNameRequest.class, projectEndpoint::changeStatusByNameProject);
        server.register(ProjectChangeStatusByIdRequest.class, projectEndpoint::changeStatusByIdProject);

        server.register(TaskListRequest.class, taskEndpoint::listTask);
        server.register(TaskListByProjectIdRequest.class, taskEndpoint::listByProjectIdTask);
        server.register(TaskCreateRequest.class, taskEndpoint::createTask);
        server.register(TaskClearRequest.class, taskEndpoint::clearTask);
        server.register(TaskUpdateByNameRequest.class, taskEndpoint::updateByNameTask);
        server.register(TaskUpdateByIdRequest.class, taskEndpoint::updateByIdTask);
        server.register(TaskListSortedRequest.class, taskEndpoint::listSortedTask);
        server.register(TaskRemoveByNameRequest.class, taskEndpoint::removeByNameTask);
        server.register(TaskRemoveByIdRequest.class, taskEndpoint::removeByIdTask);
        server.register(TaskFindByNameRequest.class, taskEndpoint::findByNameTask);
        server.register(TaskFindByIdRequest.class, taskEndpoint::findByIdTask);
        server.register(TaskBindToProjectRequest.class, taskEndpoint::bindTaskToProject);
        server.register(TaskUnbindFromProjectRequest.class, taskEndpoint::unbindTaskFromProject);
        server.register(TaskChangeStatusByNameRequest.class, taskEndpoint::changeStatusByNameTask);
        server.register(TaskChangeStatusByIdRequest.class, taskEndpoint::changeStatusByIdTask);

        server.register(DataBinarySaveRequest.class, dataEndpoint::saveBinaryData);
        server.register(DataBinaryLoadRequest.class, dataEndpoint::loadBinaryData);
        server.register(DataJsonJAXBLoadRequest.class, dataEndpoint::loadJsonJAXBData);
        server.register(DataJsonJAXBSaveRequest.class, dataEndpoint::saveJsonJAXBData);
        server.register(DataJsonFasterXMLLoadRequest.class, dataEndpoint::loadJsonFasterXMLData);
        server.register(DataJsonFasterXMLSaveRequest.class, dataEndpoint::saveJsonFasterXMLData);
        server.register(DataXmlJAXBLoadRequest.class, dataEndpoint::loadXmlJAXBData);
        server.register(DataXmlJAXBSaveRequest.class, dataEndpoint::saveXmlJAXBData);
        server.register(DataXmlFasterXMLLoadRequest.class, dataEndpoint::loadXmlFasterXMLData);
        server.register(DataXmlFasterXMLSaveRequest.class, dataEndpoint::saveXmlFasterXMLData);
        server.register(DataYamlFasterXMLLoadRequest.class, dataEndpoint::loadYamlFasterXMLData);
        server.register(DataYamlFasterXMLSaveRequest.class, dataEndpoint::saveYamlFasterXMLData);
        server.register(DataBase64SaveRequest.class, dataEndpoint::saveBase64Data);
        server.register(DataBase64LoadRequest.class, dataEndpoint::loadBase64Data);
    }

    public void run() {
        initPID();
        initUsers();
        initData();
        backup.init();
        initServer();
        server.start();
        displayWelcome();
    }

    public void displayWelcome() {
        loggerService.info("** TASK MANAGER SERVER STARTED **");
    }

}
