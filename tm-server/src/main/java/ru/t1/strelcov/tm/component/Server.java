package ru.t1.strelcov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.Operation;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.bootstrap.Bootstrap;
import ru.t1.strelcov.tm.dto.request.AbstractRequest;
import ru.t1.strelcov.tm.dto.response.AbstractResponse;
import ru.t1.strelcov.tm.task.AbstractServerTask;
import ru.t1.strelcov.tm.task.ServerAcceptTask;

import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

    @Getter
    @Nullable
    private ServerSocket serverSocket;

    @Getter
    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @NotNull
    private final Dispatcher dispatcher = new Dispatcher();

    public Server(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @SneakyThrows
    public void start() {
        @NotNull final IPropertyService propertyService = bootstrap.getPropertyService();
        @NotNull final Integer port = propertyService.getServerPort();
        serverSocket = new ServerSocket(port);
        submit(new ServerAcceptTask(this));
    }

    @SneakyThrows
    public void stop() {
        if (serverSocket == null) return;
        serverSocket.close();
        executorService.shutdown();
    }

    @SneakyThrows
    public void submit(@NotNull final AbstractServerTask abstractServerTask) {
        executorService.submit(abstractServerTask);
    }

    @NotNull
    public Object call(@NotNull final AbstractRequest request) {
        return dispatcher.call(request);
    }

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void register(@NotNull final Class<RQ> regClass, @NotNull final Operation<RQ, RS> operation) {
        dispatcher.register(regClass, operation);
    }

}
