package ru.t1.strelcov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.endpoint.IUserEndpoint;
import ru.t1.strelcov.tm.api.service.IUserService;
import ru.t1.strelcov.tm.api.service.ServiceLocator;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.model.User;

import java.util.List;

public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    @Override
    @NotNull
    public UserChangePasswordResponse changePassword(@NotNull final UserChangePasswordRequest request) {
        check(request);
        getUserService().changePasswordById(request.getUserId(), request.getPassword());
        return new UserChangePasswordResponse();
    }

    @NotNull
    @Override
    public UserListResponse listUser(@NotNull final UserListRequest request) {
        check(request, Role.ADMIN);
        @NotNull List<User> list = getUserService().findAll();
        return new UserListResponse(list);
    }

    @NotNull
    @Override
    public UserRegisterResponse registerUser(@NotNull final UserRegisterRequest request) {
        check(request, Role.ADMIN);
        @NotNull User user = getUserService().add(request.getLogin(), request.getPassword(), request.getEmail());
        return new UserRegisterResponse(user);
    }

    @NotNull
    @Override
    public UserRemoveByLoginResponse removeUserByLogin(@NotNull final UserRemoveByLoginRequest request) {
        check(request, Role.ADMIN);
        @NotNull User user = getUserService().removeByLogin(request.getLogin());
        return new UserRemoveByLoginResponse(user);
    }

    @NotNull
    @Override
    public UserUpdateByLoginResponse updateUserByLogin(@NotNull final UserUpdateByLoginRequest request) {
        check(request, Role.ADMIN);
        @NotNull User user = getUserService().updateByLogin(request.getLogin(), request.getFirstName(), request.getLastName(), null, request.getEmail());
        return new UserUpdateByLoginResponse(user);
    }


    @NotNull
    @Override
    public UserUnlockByLoginResponse unlockByLogin(@NotNull final UserUnlockByLoginRequest request) {
        check(request, Role.ADMIN);
        getUserService().unlockUserByLogin(request.getLogin());
        return new UserUnlockByLoginResponse();
    }

    @NotNull
    @Override
    public UserLockByLoginResponse lockByLogin(@NotNull final UserLockByLoginRequest request) {
        check(request, Role.ADMIN);
        getUserService().lockUserByLogin(request.getLogin());
        return new UserLockByLoginResponse();
    }

}
