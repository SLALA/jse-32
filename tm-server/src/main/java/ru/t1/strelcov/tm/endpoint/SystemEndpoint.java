package ru.t1.strelcov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.strelcov.tm.api.service.ServiceLocator;
import ru.t1.strelcov.tm.dto.request.ServerAboutRequest;
import ru.t1.strelcov.tm.dto.request.ServerVersionRequest;
import ru.t1.strelcov.tm.dto.response.ServerAboutResponse;
import ru.t1.strelcov.tm.dto.response.ServerVersionResponse;

public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    public SystemEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(serviceLocator.getPropertyService().getEmail());
        response.setName(serviceLocator.getPropertyService().getName());
        return response;
    }

    @NotNull
    @Override
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(serviceLocator.getPropertyService().getVersion());
        return response;
    }

}
