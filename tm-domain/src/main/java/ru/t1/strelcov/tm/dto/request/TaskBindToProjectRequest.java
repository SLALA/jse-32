package ru.t1.strelcov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class TaskBindToProjectRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    @Nullable
    private String taskId;

    public TaskBindToProjectRequest(@Nullable final String taskId, @Nullable final String projectId) {
        this.taskId = taskId;
        this.projectId = projectId;
    }

}
