package ru.t1.strelcov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class ProjectListSortedRequest extends AbstractUserRequest {

    @Nullable
    private String sort;

    public ProjectListSortedRequest(@Nullable final String sort) {
        this.sort = sort;
    }

}
