package ru.t1.strelcov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class ProjectCreateRequest extends AbstractUserRequest {

    @Nullable
    private String description;

    @Nullable
    private String name;

    public ProjectCreateRequest(@Nullable final String name, @Nullable final String description) {
        this.name = name;
        this.description = description;
    }

}
