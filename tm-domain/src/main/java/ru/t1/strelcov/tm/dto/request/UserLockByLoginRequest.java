package ru.t1.strelcov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class UserLockByLoginRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    public UserLockByLoginRequest(@Nullable final String login) {
        this.login = login;
    }

}
