package ru.t1.strelcov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.dto.request.UserLoginRequest;
import ru.t1.strelcov.tm.dto.request.UserLogoutRequest;
import ru.t1.strelcov.tm.dto.request.UserProfileRequest;
import ru.t1.strelcov.tm.dto.response.UserLoginResponse;
import ru.t1.strelcov.tm.dto.response.UserLogoutResponse;
import ru.t1.strelcov.tm.dto.response.UserProfileResponse;

public interface IAuthEndpoint {

    @NotNull
    UserLoginResponse login(@NotNull UserLoginRequest request);

    @NotNull
    UserLogoutResponse logout(@NotNull UserLogoutRequest request);

    @NotNull
    UserProfileResponse getProfile(@NotNull UserProfileRequest request);

}
