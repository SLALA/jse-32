package ru.t1.strelcov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;

public interface IDataEndpoint {

    @NotNull
    DataBase64LoadResponse loadBase64Data(@NotNull DataBase64LoadRequest request);

    @NotNull
    DataBase64SaveResponse saveBase64Data(@NotNull DataBase64SaveRequest request);

    @NotNull
    DataBinaryLoadResponse loadBinaryData(@NotNull DataBinaryLoadRequest request);

    @NotNull
    DataBinarySaveResponse saveBinaryData(@NotNull DataBinarySaveRequest request);

    @NotNull
    DataJsonJAXBSaveResponse saveJsonJAXBData(@NotNull DataJsonJAXBSaveRequest request);

    @NotNull
    DataJsonJAXBLoadResponse loadJsonJAXBData(@NotNull DataJsonJAXBLoadRequest request);

    @NotNull
    DataJsonFasterXMLLoadResponse loadJsonFasterXMLData(@NotNull DataJsonFasterXMLLoadRequest request);

    @NotNull
    DataXmlFasterXMLLoadResponse loadXmlFasterXMLData(@NotNull DataXmlFasterXMLLoadRequest request);

    @NotNull
    DataYamlFasterXMLSaveResponse saveYamlFasterXMLData(@NotNull DataYamlFasterXMLSaveRequest request);

    @NotNull
    DataJsonFasterXMLSaveResponse saveJsonFasterXMLData(@NotNull DataJsonFasterXMLSaveRequest request);

    @NotNull
    DataXmlFasterXMLSaveResponse saveXmlFasterXMLData(@NotNull DataXmlFasterXMLSaveRequest request);

    @NotNull
    DataYamlFasterXMLLoadResponse loadYamlFasterXMLData(@NotNull DataYamlFasterXMLLoadRequest request);

    @NotNull
    DataXmlJAXBLoadResponse loadXmlJAXBData(@NotNull DataXmlJAXBLoadRequest request);

    @NotNull
    DataXmlJAXBSaveResponse saveXmlJAXBData(@NotNull DataXmlJAXBSaveRequest request);

}
