package ru.t1.strelcov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class TaskListSortedRequest extends AbstractUserRequest {

    @Nullable
    private String sort;

    public TaskListSortedRequest(@Nullable final String sort) {
        this.sort = sort;
    }

}
