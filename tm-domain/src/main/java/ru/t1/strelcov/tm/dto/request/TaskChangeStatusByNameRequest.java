package ru.t1.strelcov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class TaskChangeStatusByNameRequest extends AbstractUserRequest {

    @Nullable
    private String status;

    @Nullable
    private String name;

    public TaskChangeStatusByNameRequest(@Nullable final String name, @Nullable final String status) {
        this.status = status;
        this.name = name;
    }

}
