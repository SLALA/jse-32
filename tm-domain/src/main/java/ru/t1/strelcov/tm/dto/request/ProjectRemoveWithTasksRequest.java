package ru.t1.strelcov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class ProjectRemoveWithTasksRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public ProjectRemoveWithTasksRequest(@Nullable final String id) {
        this.id = id;
    }

}
