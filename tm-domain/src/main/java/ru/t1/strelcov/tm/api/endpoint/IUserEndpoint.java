package ru.t1.strelcov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;

public interface IUserEndpoint {

    @NotNull
    UserChangePasswordResponse changePassword(@NotNull UserChangePasswordRequest request);

    @NotNull
    UserListResponse listUser(@NotNull UserListRequest request);

    @NotNull
    UserRegisterResponse registerUser(@NotNull UserRegisterRequest request);

    @NotNull
    UserRemoveByLoginResponse removeUserByLogin(@NotNull UserRemoveByLoginRequest request);

    @NotNull
    UserUpdateByLoginResponse updateUserByLogin(@NotNull UserUpdateByLoginRequest request);

    @NotNull
    UserUnlockByLoginResponse unlockByLogin(@NotNull UserUnlockByLoginRequest request);

    @NotNull
    UserLockByLoginResponse lockByLogin(@NotNull UserLockByLoginRequest request);

}
