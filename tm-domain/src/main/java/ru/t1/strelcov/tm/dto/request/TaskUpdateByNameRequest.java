package ru.t1.strelcov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class TaskUpdateByNameRequest extends AbstractUserRequest {

    @Nullable
    private String description;

    @Nullable
    private String name;

    @Nullable
    private String oldName;

    public TaskUpdateByNameRequest(@Nullable final String oldName, @Nullable final String name, @Nullable final String description) {
        this.oldName = oldName;
        this.name = name;
        this.description = description;
    }

}
