package ru.t1.strelcov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class UserRegisterRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    @Nullable
    private String password;

    @Nullable
    private String email;

    public UserRegisterRequest(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        this.login = login;
        this.password = password;
        this.email = email;
    }

}
