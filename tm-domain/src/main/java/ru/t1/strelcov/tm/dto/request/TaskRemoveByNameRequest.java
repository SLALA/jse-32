package ru.t1.strelcov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class TaskRemoveByNameRequest extends AbstractUserRequest {

    @Nullable
    private String name;

    public TaskRemoveByNameRequest(@Nullable final String name) {
        this.name = name;
    }

}
