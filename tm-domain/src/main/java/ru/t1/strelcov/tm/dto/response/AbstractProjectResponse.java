package ru.t1.strelcov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.model.Project;

@Setter
@Getter
@NoArgsConstructor
public abstract class AbstractProjectResponse extends AbstractResultResponse {

    @NotNull
    private Project project;

    public AbstractProjectResponse(@NotNull final Project project) {
        this.project = project;
    }

}
