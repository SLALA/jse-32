package ru.t1.strelcov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class UserUpdateByLoginRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String email;

    public UserUpdateByLoginRequest(@Nullable String login, @Nullable String firstName, @Nullable String lastName, @Nullable String email) {
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

}
