package ru.t1.strelcov.tm.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public final class UserUnlockByLoginRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    public UserUnlockByLoginRequest(@Nullable final String login) {
        this.login = login;
    }

}
