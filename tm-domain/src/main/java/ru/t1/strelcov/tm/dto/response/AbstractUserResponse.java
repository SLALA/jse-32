package ru.t1.strelcov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.model.User;

@Setter
@Getter
@NoArgsConstructor
public abstract class AbstractUserResponse extends AbstractResultResponse {

    @NotNull
    private User user;

    public AbstractUserResponse(@NotNull final User user) {
        this.user = user;
    }

}
