package ru.t1.strelcov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;

public interface IProjectEndpoint {

    @NotNull
    ProjectListResponse listProject(@NotNull ProjectListRequest request);

    @NotNull
    ProjectListSortedResponse listSortedProject(@NotNull ProjectListSortedRequest request);

    @NotNull
    ProjectChangeStatusByIdResponse changeStatusByIdProject(@NotNull ProjectChangeStatusByIdRequest request);

    @NotNull
    ProjectChangeStatusByNameResponse changeStatusByNameProject(@NotNull ProjectChangeStatusByNameRequest request);

    @NotNull
    ProjectClearResponse clearProject(@NotNull ProjectClearRequest request);

    @NotNull
    ProjectCreateResponse createProject(@NotNull ProjectCreateRequest request);

    @NotNull
    ProjectFindByIdResponse findByIdProject(@NotNull ProjectFindByIdRequest request);

    @NotNull
    ProjectFindByNameResponse findByNameProject(@NotNull ProjectFindByNameRequest request);

    @NotNull
    ProjectRemoveByIdResponse removeByIdProject(@NotNull ProjectRemoveByIdRequest request);

    @NotNull
    ProjectRemoveByNameResponse removeByNameProject(@NotNull ProjectRemoveByNameRequest request);

    @NotNull
    ProjectRemoveWithTasksResponse removeWithTasksProject(@NotNull ProjectRemoveWithTasksRequest request);

    @NotNull
    ProjectUpdateByIdResponse updateByIdProject(@NotNull ProjectUpdateByIdRequest request);

    @NotNull
    ProjectUpdateByNameResponse updateByNameProject(@NotNull ProjectUpdateByNameRequest request);

}
