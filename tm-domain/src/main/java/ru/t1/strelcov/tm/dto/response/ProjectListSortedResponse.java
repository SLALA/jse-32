package ru.t1.strelcov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.model.Project;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public final class ProjectListSortedResponse extends AbstractResultResponse {

    @NotNull
    private List<Project> list;

    public ProjectListSortedResponse(@NotNull List<Project> list) {
        this.list = list;
    }

}
