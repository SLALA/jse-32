package ru.t1.strelcov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;

public interface ITaskEndpoint {

    @NotNull
    TaskListResponse listTask(@NotNull TaskListRequest request);

    @NotNull
    TaskListSortedResponse listSortedTask(@NotNull TaskListSortedRequest request);

    @NotNull
    TaskListByProjectIdResponse listByProjectIdTask(@NotNull TaskListByProjectIdRequest request);

    @NotNull
    TaskChangeStatusByIdResponse changeStatusByIdTask(@NotNull TaskChangeStatusByIdRequest request);

    @NotNull
    TaskChangeStatusByNameResponse changeStatusByNameTask(@NotNull TaskChangeStatusByNameRequest request);

    @NotNull
    TaskClearResponse clearTask(@NotNull TaskClearRequest request);

    @NotNull
    TaskCreateResponse createTask(@NotNull TaskCreateRequest request);

    @NotNull
    TaskFindByIdResponse findByIdTask(@NotNull TaskFindByIdRequest request);

    @NotNull
    TaskFindByNameResponse findByNameTask(@NotNull TaskFindByNameRequest request);

    @NotNull
    TaskRemoveByIdResponse removeByIdTask(@NotNull TaskRemoveByIdRequest request);

    @NotNull
    TaskRemoveByNameResponse removeByNameTask(@NotNull TaskRemoveByNameRequest request);

    @NotNull
    TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request);

    @NotNull
    TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull TaskUnbindFromProjectRequest request);

    @NotNull
    TaskUpdateByIdResponse updateByIdTask(@NotNull TaskUpdateByIdRequest request);

    @NotNull
    TaskUpdateByNameResponse updateByNameTask(@NotNull TaskUpdateByNameRequest request);

}
