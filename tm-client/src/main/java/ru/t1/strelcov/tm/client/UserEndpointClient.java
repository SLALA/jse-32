package ru.t1.strelcov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.IUserEndpoint;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;

import java.net.Socket;

@NoArgsConstructor
public final class UserEndpointClient extends AbstractEndpointClient implements IUserEndpoint {

    public UserEndpointClient(@Nullable final Socket socket) {
        super(socket);
    }

    @NotNull
    @Override
    public UserChangePasswordResponse changePassword(@NotNull UserChangePasswordRequest request) {
        return call(request, UserChangePasswordResponse.class);
    }

    @NotNull
    @Override
    public UserListResponse listUser(@NotNull UserListRequest request) {
        return call(request, UserListResponse.class);
    }

    @NotNull
    @Override
    public UserRegisterResponse registerUser(@NotNull UserRegisterRequest request) {
        return call(request, UserRegisterResponse.class);
    }

    @NotNull
    @Override
    public UserRemoveByLoginResponse removeUserByLogin(@NotNull UserRemoveByLoginRequest request) {
        return call(request, UserRemoveByLoginResponse.class);
    }

    @NotNull
    @Override
    public UserUpdateByLoginResponse updateUserByLogin(@NotNull UserUpdateByLoginRequest request) {
        return call(request, UserUpdateByLoginResponse.class);
    }

    @NotNull
    @Override
    public UserUnlockByLoginResponse unlockByLogin(@NotNull UserUnlockByLoginRequest request) {
        return call(request, UserUnlockByLoginResponse.class);
    }

    @NotNull
    @Override
    public UserLockByLoginResponse lockByLogin(@NotNull UserLockByLoginRequest request) {
        return call(request, UserLockByLoginResponse.class);
    }

}
