package ru.t1.strelcov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.dto.request.DataJsonFasterXMLLoadRequest;
import ru.t1.strelcov.tm.enumerated.Role;

public class DataJsonLoadFasterXMLCommand extends AbstractDataCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String name() {
        return "data-json-load-fasterxml";
    }

    @Override
    @NotNull
    public String description() {
        return "Load entities data from json file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA JSON LOAD]");
        serviceLocator.getDataEndpoint().loadJsonFasterXMLData(new DataJsonFasterXMLLoadRequest());
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
