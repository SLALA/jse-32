package ru.t1.strelcov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.strelcov.tm.dto.request.ServerAboutRequest;
import ru.t1.strelcov.tm.dto.request.ServerVersionRequest;
import ru.t1.strelcov.tm.dto.response.ServerAboutResponse;
import ru.t1.strelcov.tm.dto.response.ServerVersionResponse;

import java.net.Socket;

@NoArgsConstructor
public final class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpoint {

    public SystemEndpointClient(@Nullable final Socket socket) {
        super(socket);
    }

    @Override
    @NotNull
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        return call(request, ServerAboutResponse.class);
    }

    @Override
    @NotNull
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        return call(request, ServerVersionResponse.class);
    }

    public static void main(@Nullable final String[] args) {
        @NotNull final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        System.out.println("[VERSION]:\n");
        @NotNull final ServerVersionResponse versionResponse = client.getVersion(new ServerVersionRequest());
        System.out.println(versionResponse.getVersion());
        System.out.println("[ABOUT]:\n");
        @NotNull final ServerAboutResponse aboutResponse = client.getAbout(new ServerAboutRequest());
        System.out.println(aboutResponse.getName() + '\n' + aboutResponse.getEmail());
        client.disconnect();
    }

}
