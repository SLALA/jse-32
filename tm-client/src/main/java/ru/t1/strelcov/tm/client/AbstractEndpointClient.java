package ru.t1.strelcov.tm.client;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.IEndpointClient;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.dto.response.ServerErrorResponse;
import ru.t1.strelcov.tm.exception.system.NoConnectionException;
import ru.t1.strelcov.tm.service.PropertyService;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

@NoArgsConstructor
@Getter
@Setter
public abstract class AbstractEndpointClient implements IEndpointClient {

    @Nullable
    private Socket socket;

    public AbstractEndpointClient(@Nullable Socket socket) {
        this.socket = socket;
    }

    @NotNull
    @SneakyThrows
    @SuppressWarnings("unchecked")
    protected <T> T call(@NotNull final Object data, @NotNull Class<T> clazz) {
        if (socket == null || socket.isClosed()) throw new NoConnectionException();
        new ObjectOutputStream(socket.getOutputStream()).writeObject(data);
        @NotNull Object result = new ObjectInputStream(socket.getInputStream()).readObject();
        if (result instanceof ServerErrorResponse)
            throw new Throwable(((ServerErrorResponse) result).getMessage());
        return (T) result;
    }

    @SneakyThrows
    public void connect() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        socket = new Socket(propertyService.getServerHost(), propertyService.getServerPort());
    }

    @SneakyThrows
    public void disconnect() {
        if (socket == null) return;
        socket.close();
    }

}
