package ru.t1.strelcov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.strelcov.tm.dto.request.ProjectUpdateByIdRequest;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-update-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Update project by id.";
    }

    @Override
    public void execute() {
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NEW NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectEndpoint.updateByIdProject(new ProjectUpdateByIdRequest(id, name, description)).getProject();
        showProject(project);
    }

}
