package ru.t1.strelcov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.strelcov.tm.dto.request.ProjectRemoveWithTasksRequest;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.util.TerminalUtil;

public final class ProjectRemoveWithTasksCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-remove-with-tasks";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project with bound tasks.";
    }

    @Override
    public void execute() {
        @NotNull final IProjectEndpoint projectTaskEndpoint = serviceLocator.getProjectEndpoint();
        System.out.println("[REMOVE PROJECT WITH TASKS]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        final Project project = projectTaskEndpoint.removeWithTasksProject(new ProjectRemoveWithTasksRequest(id)).getProject();
    }

}
