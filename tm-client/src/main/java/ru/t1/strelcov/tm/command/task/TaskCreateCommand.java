package ru.t1.strelcov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.strelcov.tm.dto.request.TaskCreateRequest;
import ru.t1.strelcov.tm.model.Task;
import ru.t1.strelcov.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create task.";
    }

    @Override
    public void execute() {
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER TASK NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        @NotNull final Task task = taskEndpoint.createTask(new TaskCreateRequest(name, description)).getTask();
        showTask(task);
    }

}
