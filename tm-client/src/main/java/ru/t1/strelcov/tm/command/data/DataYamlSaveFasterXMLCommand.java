package ru.t1.strelcov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.dto.request.DataYamlFasterXMLSaveRequest;
import ru.t1.strelcov.tm.enumerated.Role;

public class DataYamlSaveFasterXMLCommand extends AbstractDataCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String name() {
        return "data-yaml-save-fasterxml";
    }

    @Override
    @NotNull
    public String description() {
        return "Save entities data to yaml file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA YAML SAVE]");
        serviceLocator.getDataEndpoint().saveYamlFasterXMLData(new DataYamlFasterXMLSaveRequest());
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
