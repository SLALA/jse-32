package ru.t1.strelcov.tm.command.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.IEndpointClient;
import ru.t1.strelcov.tm.command.AbstractCommand;

import java.net.Socket;

public class ConnectCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "connect";
    }

    @Override
    public @NotNull String description() {
        return "Connect to socket server.";
    }

    @Override
    public void execute() {
        ((IEndpointClient) serviceLocator.getAuthEndpoint()).connect();
        @Nullable final Socket socket = ((IEndpointClient) serviceLocator.getAuthEndpoint()).getSocket();
        ((IEndpointClient) serviceLocator.getAuthEndpoint()).setSocket(socket);
        ((IEndpointClient) serviceLocator.getSystemEndpoint()).setSocket(socket);
        ((IEndpointClient) serviceLocator.getUserEndpoint()).setSocket(socket);
        ((IEndpointClient) serviceLocator.getProjectEndpoint()).setSocket(socket);
        ((IEndpointClient) serviceLocator.getTaskEndpoint()).setSocket(socket);
        ((IEndpointClient) serviceLocator.getDataEndpoint()).setSocket(socket);
    }

}
