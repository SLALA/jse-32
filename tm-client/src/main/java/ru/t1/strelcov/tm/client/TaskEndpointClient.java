package ru.t1.strelcov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;

import java.net.Socket;

@NoArgsConstructor
public final class TaskEndpointClient extends AbstractEndpointClient implements ITaskEndpoint {

    public TaskEndpointClient(@Nullable final Socket socket) {
        super(socket);
    }

    @NotNull
    @Override
    public TaskListResponse listTask(@NotNull TaskListRequest request) {
        return call(request, TaskListResponse.class);
    }

    @NotNull
    @Override
    public TaskListSortedResponse listSortedTask(@NotNull TaskListSortedRequest request) {
        return call(request, TaskListSortedResponse.class);
    }

    @Override
    public @NotNull TaskListByProjectIdResponse listByProjectIdTask(@NotNull TaskListByProjectIdRequest request) {
        return call(request, TaskListByProjectIdResponse.class);
    }

    @NotNull
    @Override
    public TaskChangeStatusByIdResponse changeStatusByIdTask(@NotNull TaskChangeStatusByIdRequest request) {
        return call(request, TaskChangeStatusByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskChangeStatusByNameResponse changeStatusByNameTask(@NotNull TaskChangeStatusByNameRequest request) {
        return call(request, TaskChangeStatusByNameResponse.class);
    }

    @NotNull
    @Override
    public TaskClearResponse clearTask(@NotNull TaskClearRequest request) {
        return call(request, TaskClearResponse.class);
    }

    @NotNull
    @Override
    public TaskCreateResponse createTask(@NotNull TaskCreateRequest request) {
        return call(request, TaskCreateResponse.class);
    }

    @NotNull
    @Override
    public TaskFindByIdResponse findByIdTask(@NotNull TaskFindByIdRequest request) {
        return call(request, TaskFindByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskFindByNameResponse findByNameTask(@NotNull TaskFindByNameRequest request) {
        return call(request, TaskFindByNameResponse.class);
    }

    @NotNull
    @Override
    public TaskRemoveByIdResponse removeByIdTask(@NotNull TaskRemoveByIdRequest request) {
        return call(request, TaskRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskRemoveByNameResponse removeByNameTask(@NotNull TaskRemoveByNameRequest request) {
        return call(request, TaskRemoveByNameResponse.class);
    }

    @NotNull
    @Override
    public TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request) {
        return call(request, TaskBindToProjectResponse.class);
    }

    @NotNull
    @Override
    public TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull TaskUnbindFromProjectRequest request) {
        return call(request, TaskUnbindFromProjectResponse.class);
    }

    @NotNull
    @Override
    public TaskUpdateByIdResponse updateByIdTask(@NotNull TaskUpdateByIdRequest request) {
        return call(request, TaskUpdateByIdResponse.class);
    }

    @NotNull
    @Override
    public TaskUpdateByNameResponse updateByNameTask(@NotNull TaskUpdateByNameRequest request) {
        return call(request, TaskUpdateByNameResponse.class);
    }

}
