package ru.t1.strelcov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;

import java.net.Socket;

public interface IEndpointClient {

    void connect();

    void disconnect();

    @Nullable
    Socket getSocket();

    void setSocket(@Nullable Socket socket);

}
