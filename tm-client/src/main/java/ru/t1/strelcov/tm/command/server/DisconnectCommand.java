package ru.t1.strelcov.tm.command.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.IEndpointClient;
import ru.t1.strelcov.tm.command.AbstractCommand;

public class DisconnectCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "disconnect";
    }

    @NotNull
    @Override
    public String description() {
        return "Disconnect from socket server.";
    }

    @Override
    public void execute() {
        ((IEndpointClient) serviceLocator.getAuthEndpoint()).disconnect();
    }

}
