package ru.t1.strelcov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.repository.ICommandRepository;
import ru.t1.strelcov.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @NotNull
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @Nullable
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return commands.get(name);
    }

    @Nullable
    public AbstractCommand getCommandByArg(@NotNull final String arg) {
        return arguments.get(arg);
    }

    public void add(@NotNull final AbstractCommand command) {
        @NotNull final String name = command.name();
        @Nullable final String arg = command.arg();
        Optional.of(name).ifPresent((optionalName) -> commands.put(optionalName, command));
        Optional.ofNullable(arg).ifPresent((optionalArg) -> arguments.put(optionalArg, command));
    }

}
