package ru.t1.strelcov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.strelcov.tm.dto.request.ProjectClearRequest;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Create project.";
    }

    @Override
    public void execute() {
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        System.out.println("[TASK CLEAR]");
        projectEndpoint.clearProject(new ProjectClearRequest());
    }

}
